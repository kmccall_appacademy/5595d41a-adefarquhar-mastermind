require 'byebug'

class Code
  attr_reader :pegs

  PEGS = { red: 'r', green: 'g', blue: 'b', yellow: 'y',
           orange: 'o', purple: 'p' }

  def initialize(pegs)
    raise "pegs require an array" unless pegs.is_a? Array
    @pegs = pegs
  end

  def self.parse(string)
    valid_pegs = PEGS.values
    code = string.downcase.chars

    code.each do |peg|
      raise "invalid code" unless valid_pegs.include?(peg)
    end

    Code.new(code)
  end

  def self.random
    rand_pegs = []
    4.times { |nth| rand_pegs << PEGS.values.sample }
    Code.new(rand_pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(code2)
    matches = 0
    pegs.each_index do |idx|
      matches += 1 if pegs[idx] == code2[idx]
    end

    matches
  end

  def near_matches(code2)
    exact = exact_matches(code2)
    total = total_matches(code2)

    near = total - exact
  end

  # helper method for near_matches
  def total_matches(code2)
    # debugger
    color_ct1 = Hash.new(0)
    color_ct2 = Hash.new(0)
    pegs.each { |clr| color_ct1[clr] += 1 }
    code2.pegs.each { |clr| color_ct2[clr] += 1 }

    color_ct2.select! { |key, clr| key if color_ct1.key?(key) }
    return 0 if color_ct2.empty?
    color_ct1.select! { |key, clr| key if color_ct2.key?(key) }

    total = color_ct1.merge(color_ct2) do |key, ct1, ct2|
      ct1 < ct2 ? ct1 : ct2
    end

    total.values.reduce(:+)
  end

  def ==(code2)
    return true if (code2.is_a? Code) && exact_matches(code2) == 4
    false
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Colors - red, green, blue, yellow, orange, purple"

    puts "Enter a code (e.g. \"bryg\"):"
    guess = $stdin.gets.chomp

    Code.parse(guess)
  end

  def display_matches(code)
    guess_code = code
    exact = secret_code.exact_matches(guess_code)
    near = secret_code.near_matches(guess_code)
    puts "You got #{near} near matches & #{exact} exact matches."
  end

  def play
    puts "After 10 guesses, the game will end. Good Luck!"

    (1..10).each do |time|
      puts "This is guess #{time}..."
      guess_code = get_guess
      display_matches(guess_code)
      return puts "Congrats! You won!" if won?(guess_code)
    end

    puts "You ran out of turns..."
  end

  def won?(guess_code)
    return true if secret_code== guess_code
    false
  end
end
